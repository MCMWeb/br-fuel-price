import re
import time
from pprint import pprint

import requests
from bs4 import BeautifulSoup

ANP_BASE_URL = 'http://anp.gov.br/preco/'
OCR_BASE_URL = 'https://api.cloudmersive.com/ocr/'
CAPTCHA_INPUT_ID = 'txtValor'
OCR_REGEX = re.compile(r'\b\w{5,7}\b')


class ANPClient(object):

    def __init__(self, cloudmersive_api_key):
        self.anp = requests.session()
        self.anp.headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36'

        self.cloudmersive = requests.session()
        self.cloudmersive.headers['Apikey'] = cloudmersive_api_key

    def get(self):
        pass

    def get_captcha(self):
        # home = self.anp.get(ANP_BASE_URL + 'prc/Resumo_Mensal_Index.asp')  # init session
        # if home.status_code != 200:
        #     print('ANP website is unavailable! Status code: %i -- %s' % (home.status_code, home.text))
        #     return None
        captcha = self.anp.get(ANP_BASE_URL + 'prc/imagem.asp')
        with open('tmp.gif', 'wb') as tmp_captcha:
            tmp_captcha.write(captcha.content)
        return 'anp_captcha.gif', captcha.content

    def ocr_captcha(self):
        filtered_text = None
        tries = 0
        while tries < 10:
            tries += 1
            captcha = self.get_captcha()
            if not captcha:
                print('Unable to fetch captcha!')
                continue
            response = self.cloudmersive.post(OCR_BASE_URL + 'image/toText', files={'imageFile': captcha}).json()
            confidence = response.get('MeanConfidenceLevel', 0)
            text = response.get('TextResult', '').strip()
            print('Confidence level: %f, OCR result: "%s"' % (confidence, text))
            if text:
                text_matcher = OCR_REGEX.match(text)
                if text_matcher:
                    filtered_text = text_matcher.group(0)
                    print('Original result: "%s", filtered result: "%s"' % (text, filtered_text))
                    break
            print('No valid text found')
            time.sleep(0.4)  # avoid OCR throttling
        return filtered_text

    def summary(self):
        # data = {}
        # home_response = self.anp.get(ANP_BASE_URL + 'prc/Resumo_Semanal_Index.asp')
        # home_tree = BeautifulSoup(home_response.text, 'html.parser')
        # hidden_fields = home_tree.find_all('input', attrs={'type': 'hidden'})
        # for hidden_field in hidden_fields:
        #     data[hidden_field.attrs['name']] = hidden_field.attrs['value']
        # data['rdResumo'] = 0
        # data['txtValor'] = self.ocr_captcha()
        # table_response = self.anp.post(ANP_BASE_URL + 'prc/Resumo_Semanal_Index.asp', data=data)
        # with open('tmp.html', 'w') as tmp_html:
        #     tmp_html.write(table_response.text)
        # table_tree = BeautifulSoup(table_response.text, 'html.parser')
        response = {}
        table_tree = BeautifulSoup(open('tmp.html', 'r').read(), 'html.parser').find('table', attrs={'class': 'table_padrao'})
        rows = table_tree.find_all('tr', recursive=False)
        response['scope'] = rows[0].find('th').text.strip()

        titles = []
        subtitles = [subtitle.text.strip().capitalize() for subtitle in rows[2].find_all('th')]
        for title in rows[1].find_all(attrs={'align': 'center'}):
            sane_title = ' '.join(title.text.replace('n º de', '').split()).capitalize()
            subtitle_count = int(title.attrs.get('colspan', '0'))
            if subtitle_count:
                titles.append({'title': sane_title, 'subtitles': subtitles[0:subtitle_count]})
                del subtitles[0:subtitle_count]
            else:
                titles.append({'title': sane_title})


if __name__ == '__main__':
    anp = ANPClient('7f2864fe-e73b-40f7-8f37-90eeea4ec4d0')
    # print(anp.ocr_captcha())
    anp.summary()
